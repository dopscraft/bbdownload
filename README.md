# BBDownload #

Downloads a bitbucket repo for OpenComputers.
Depends on parts of OpenOS.

### Installation ###

In OpenOS, with an internet card, type:

`wget https://bitbucket.org/dopscraft/bbdownload/raw/HEAD/bbdownload.lua`

### Usage ###

`bbdownload <project>/<repo> [<dest_folder>]`

For example:

`bbdownload dopscraft/miningplus mining-plus`

If the destination path starts with `/` then the path is absolute,
otherwise it's relative to the current directory.
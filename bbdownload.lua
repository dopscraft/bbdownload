local internet = require("internet")
local io = require("io")
local fs = require("filesystem")
local shell = require("shell")

local args = { ... }
local arg = 1
local filelist = nil
local branch = "HEAD"
local source = nil
local dest = nil

local function usage()
  print("Usage:")
  print("  bbdownload [ OPTIONS ] <project>/<repo> <destination>")
  print("Options:")
  print("  -l <file>")
  print("     Use the specified local filelist instead of downloading from the repo")
  print("  -b <branch>")
  print("     Download the given branch")
  return
end

if args[1] == nil then
  usage()
  return
end

-- Reads filelist from named file
-- Returns new filelist, or nil
local function readFilelist(file)
  local filelist = {}
  local file = io.open(file, "r")
  if file == nil then
    return nil
  end
  for line in file:lines() do
    table.insert(filelist, line)
  end
  file:close()
  return filelist
end

-- Wraps the repo request
local function getRepoFile(file)
  local url = "https://bitbucket.org/" .. source .. "/raw/" .. branch .. "/" .. file
  return internet.request(url)
end

-- Downloads a file
-- Returns true on success, false on failure
local function downloadFile(file, destPath)
  local success, repoFile = pcall(getRepoFile, file)
  if not success then
    print("Error downloading " .. file .. ": " .. repoFile)
    return false
  end
  destPath = shell.resolve(destPath)
  fs.makeDirectory(fs.path(destPath))
  local localFile = assert(io.open(destPath, "w"))
  for chunk in repoFile do
    localFile:write(chunk)
  end
  localFile:close()
  return true
end

-- Process params
while args[arg] ~= nil and string.sub(args[arg], 1, 1) == "-" do
  if args[arg] == "-l" then
    arg = arg + 1
    filelist = readFilelist(args[arg])
    if filelist == nil then
      print("Error: could not read filelist from " .. args[arg])
      return
    end
    readFilelist(file:lines())
    file:close()
  elseif args[arg] == "-b" then
    arg = arg + 1
    branch = args[arg]
  end
  arg = arg + 1
end

if args[arg] == nil then
  print("Missing <project>/<repo>")
  usage()
  return
end

source = args[arg]
dest = args[arg + 1]
if dest == nil then
  dest = source
end

-- Download filelist from repo if it wasn't a parameter
if filelist == nil then
  if not downloadFile("filelist.txt", "/tmp/filelist.txt") then
    print("Error: could not download filelist from repo")
    return
  end
  filelist = readFilelist("/tmp/filelist.txt")
  if filelist == nil then
    print("Error: could not read filelist downloaded from repo")
    return
  end
end

-- download every file in the filelist from the repo
print("Downloading...")
for k, line in pairs(filelist) do
  print(line .. " -> " .. dest .. "/" .. line)
  downloadFile(line, dest .. "/" .. line)
end
print("Done")
